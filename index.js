import $ from "jquery";

//The calculator constructor
function Calculator() {

}

//This prototype function is used to add the frame of the calculator
Calculator.prototype.addFrame = function () {
    let frame = `<div class='main-frame'></div>`
    $('body').html(frame);
}

//This prototype function is used to add the screen of the calculator
Calculator.prototype.addScreen = function () {
    let screen = `<div class='outer-screen'><div class='screen'></div></div>`
    $('.main-frame').html(screen);
}

//This prototype function is used to add all of the buttons of the calculator
Calculator.prototype.addButtons = function () {
    let buttons = 
    `<div class='buttons-wrapper'>
        <div class='button number'>7</div>
        <div class='button number'>8</div>
        <div class='button number'>9</div>
        <div class='button remove'>←</div>

        <div class='button number'>4</div>
        <div class='button number'>5</div>
        <div class='button number'>6</div>
        <div class='button clear'>C</div>

        <div class='button number'>1</div>
        <div class='button number'>2</div>
        <div class='button number'>3</div>
        <div class='button operator'>÷</div>

        <div class='button number'>0</div>
        <div class='button number'>.</div>
        <div class='button operator'>x</div>
        <div class='button operator'>-</div>
        
        <div class='button triple-button equals'>=</div>
        <div class='button operator'>+</div>
        
    </div>`
    $('.main-frame').append(buttons);
}

////This prototype function is used to bind all the keys to do something in the calculator
Calculator.prototype.addEventListeners = function () {
    //Selecting the screen
    let screen = $('.screen')

    //Adding a number to the screen
    $(document).on('click', '.button.number', function () {
        if(screen.hasClass('ans')){
            screen.removeClass('ans');
            screen.text($(this).text());
        }else{
            screen.append($(this).text());
        }
    })

    //Adding an operator to the screen
    $(document).on('click', '.button.operator', function () {
        let lastVal = screen.text().substr(-1);
        if(lastVal == '+' || lastVal == '-' || lastVal == 'x' || lastVal == '÷'){
        } else {
            screen.append($(this).text());
        }
        screen.removeClass('ans');
    })

    //Get the equation, work out the answer and put it on the screen
    $(document).on('click', '.button.equals', function () {
        let newVal = screen.text().replace(/x/g, '*').replace(/÷/g, '/');
        let ans = eval(newVal);
        screen.text(+ans.toFixed(2));
        screen.addClass('ans');
    })

    //Remove the last character of the screen
    $(document).on('click', '.button.remove', function () {
        screen.text(screen.text().slice(0, -1));
    })

    //Clear the screen
    $(document).on('click', '.button.clear', function () {
        screen.text("");
        screen.removeClass('ans');
    })
}

//Add keyboard functionality to the calculator
Calculator.prototype.addKeyboardFunctionality = function () {
    //Selecting the screen
    let screen = $('.screen')
        
    $(document).keypress(function(evt) {
        //The key value
        let key = evt.key;
    
        //This handels the numbers
        if(Number.isInteger(Number.parseInt(key))){
            if(screen.hasClass('ans')){
                screen.removeClass('ans');
                screen.text(key);
            }else{
                screen.append(key);
            }
        }
        
        //This handels the operators
        if(key == '+' || key == '-' || key == '*' || key == '/'){
            screen.removeClass('ans');
            screen.append(key);
        }
    
        //This handels the equals
        if(key == 'Enter' || key == "="){
            let newVal = screen.text().replace(/x/g, '*').replace(/÷/g, '/');
            let ans = eval(newVal);
            screen.text(+ans.toFixed(2));
            screen.addClass('ans');        
        }

        //I could not get backspace to work. It is as if I do not click anyhing, no event, nothing. 
    });
}

//Load the calculator on page load
$('document').ready(function () {
    let myCalc = new Calculator()
    myCalc.addFrame()
    myCalc.addScreen()
    myCalc.addButtons()
    myCalc.addEventListeners()
    myCalc.addKeyboardFunctionality()
});

window.Calculator = Calculator;